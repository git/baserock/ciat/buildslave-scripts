#!/bin/sh

# Copyright (C) 2015  Codethink Limited
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 2 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program.  If not, see <http://www.gnu.org/licenses/>.

# Script to deploy a cluster

set -ex

cluster_name="$1"
definitions_dir=$(pwd)/definitions
ybd_dir=$(pwd)/ybd
ybd_base=/src

# Can only deploy with a single instance of ybd. Crashes
# with >1
sudo sed -i '/^instances: /c\instances: 1' "$ybd_dir"/ybd.conf

# Make sure the btrfs module is loaded else all deploys shall be in vain
sudo modprobe btrfs

# try changing to the definitions dir then building
# ybd seems to set the "target" to be current dir, regardless
# Of it being definitions or not

cd "$definitions_dir"

sudo "$ybd_dir"/ybd.py "$cluster_name" 


# Hmm, remove built system artifacts, tar and .unpacked
# They are bloat and not needed after the deploy As they have
# the image is made and has been archived.

# get the system name 
s_n=`awk '/morph:/ {print $3}' "$cluster_name" | cut -d'/' -f2 | cut -d'.' -f1`

# Remove the artifacts 
sudo rm -rf "$ybd_base"/artifacts/"$s_n".*
