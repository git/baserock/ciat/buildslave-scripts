#!/bin/sh

# Copyright (C) 2015  Codethink Limited
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 2 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program.  If not, see <http://www.gnu.org/licenses/>.

# Script to build a defined set of definitions

set -ex

system_name="$1"
system_arch="$2"

definitions_dir="$(pwd)/definitions"
ybd_dir="$(pwd)/ybd"

# Experimentally we have seen no benefit to a single build with greater
# than max-jobs: 10, though 8 divides up more nicely, so we previously
# had 5 builders with 8 cores each for the parallel build on the meaty
# x86 VPS, but we'd like to see some parallelism on the ARM builders,
# which have 8 CPUs total, so we'd need to have 4 to see any parallelism.
cpus_per_instance=8
ncpus="$(nproc)"

# Determine the number of instances to have such that each has at least
# the specified number of CPUs but not more than double it.
# STYLE NOTE: Substitutions are intentionally not quoted here, since in
# some shells arithmetic expansion doesn't follow the traditional POSIX
# word splitting, so quotes aren't removed.
instances="$(expr "$ncpus" / "$cpus_per_instance")"
if [ "$instances" -eq 0 ]; then
    instances=1
fi
sudo sed -i "/^instances: /c\\instances: $instances" "$ybd_dir"/ybd.conf

# will this build things - No. 
# "$ybd_dir"/ybd.py "$definitions_dir"/systems/"$system_name" 

# try changing to the definitions dir then building
# ybd seems to set the "target" to be current dir, regardless

cd "$definitions_dir"

sudo "$ybd_dir"/ybd.py systems/"$system_name" "$system_arch"
