#!/bin/sh
# Copyright (C) 2015  Codethink Limited
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 2 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program.  If not, see <http://www.gnu.org/licenses/>.

# Script to set up yum-y or apt-y machine for kbas use

if [ -x /usr/bin/apt ]; then
    apt-get install -y gawk python-pip 
elif [ -x /usr/bin/yum ]; then
    yum install -y gawk python-pip
else
    echo "Unsuported package manager, try apt or yum."
    exit 1
fi

# TODO: Check if this is all that a cache server will need. unsure as running
# the cache and ybd on the same machine currently
pip install bottle

# TODO: clone a copy of byd to this user and check out a necessary version.
# Then copy in the kbas config from buildscripts


# Now install the init script for the kbas server and start it
cp init_scripts/artifact_server /etc/init.d/.
chkconfig --add artifact_server

/etc/init.d/artifact_server start
