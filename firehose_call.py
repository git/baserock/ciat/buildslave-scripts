
# Copyright (C) 2015  Codethink Limited
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 2 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program.  If not, see <http://www.gnu.org/licenses/>.

# takes a repo as $1 and calls firehose for each config the repo appears in
import os

FIREHOSE_DIR = 'src/firehose'
LOGFILE = os.path.expanduser("~/orchestration/trigger_log")

from firehose_config import get_landings

log_file = open(LOGFILE,'a')

def log(msg):
    ''' write message to log file with timestamp and script name '''
    import datetime
    global log_file
    dt = str(datetime.datetime.now()).split('.')[0]
    log_file.write("[%s] Firehose Trigger: %s\n" % (dt, msg))

def firehose_cmd(config,loglevel='debug',_log='/dev/stdout'):
    ''' take list of firehose configs filename and return a firehose command with that
    config '''
    cmd = [
            "./firehose.sh",
            "--log-level=%s" % loglevel,
            "--log=%s" % _log,
        ] + config
    log(cmd)
    return cmd

if __name__ == '__main__':
    import sys, subprocess, os

    try:
        repo = sys.argv[1]
    except:
        log("Called without being given a repo")
        print "firehose_map.py requires a repo argument"
        exit(1)

    landings = get_landings(FIREHOSE_DIR)

    for configs in landings:
        if not configs: continue
        exit_val = subprocess.call(firehose_cmd(configs), cwd=FIREHOSE_DIR)
        if not exit_val:
            log("Successfully configured %s" % ', '.join(configs))
        else:
            log("Unsuccessful")
            exit(exit_val)
