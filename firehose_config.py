
# Copyright (C) 2015  Codethink Limited
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 2 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program.  If not, see <http://www.gnu.org/licenses/>.

from os import listdir
from os.path import join


LANDING_DIRS = ("examples", "genivi", "armv8")


def get_landings(firehose_dir):
    for landing_dir in LANDING_DIRS:
        yield sorted(join(landing_dir, fname)
                     for fname in listdir(join(firehose_dir, landing_dir))
                     if fname.endswith('.yaml'))


__all__ = ['get_landings']
