
# Copyright (C) 2015  Codethink Limited
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 2 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program.  If not, see <http://www.gnu.org/licenses/>.

import subprocess, os

ORCHE_URL = 'http://52.19.1.31:8080/'
DEFAULT_SYSTEM='genivi-demo-platform-x86_64-generic.morph'
BUILD_SCRIPT = 'build_a_system.sh'
DEPLOY_SCRIPT = 'deploy_a_system.sh'
DEFINITIONS_DIR='definitions'
DEFAULT_REF = "cu010-trove/br6/firehose-test-1"
TESTING_REPO = 'ssh://git@git.baserock.org/baserock/ciat/ciat-tester'
BUILDSLAVE_SCRIPTS_REPO = 'ssh://git@git.baserock.org/baserock/ciat/buildslave-scripts'

whitelist = [
        DEFAULT_SYSTEM,
        'clusters/ciat-test-cluster.morph',
        'strata/build-essential.morph',
        'strata/core.morph',
        'strata/foundation.morph',
        'strata/bsp-x86_64-generic.morph',
    ]

LOGFILE = os.path.expanduser("~/orchestration/trigger_log")

log_file = open(LOGFILE,'a')

def log(msg):
    ''' write message to log file with timestamp and script name '''
    import datetime
    global log_file
    dt = str(datetime.datetime.now()).split('.')[0]
    log_file.write("[%s] Builder Trigger: %s\n" % (dt, msg))

def _exit(exit_val):
    if exit_val: log('exiting unhappily')
    exit(exit_val)

def files_changed(ref):
    ''' return a list of files changed in latest commit to definitions'''
    import os
    owd = os.getcwd()
    os.chdir(DEFINITIONS_DIR)
    SHAcmd = ['git', 'log', ref, '--format=format:%H', '-2']
    SHAproc = subprocess.Popen(SHAcmd, stdout=subprocess.PIPE)
    SHAout, SHAerr = SHAproc.communicate()
    SHA = SHAout.split()
    cmd = ['git', 'diff', '--name-only', SHA[0], SHA[1]]
    p = subprocess.Popen(cmd, stdout=subprocess.PIPE)
    out, err = p.communicate()
    os.chdir(owd)
    return out.split()

def find_systems_affected_by_change():
    # TODO for each file changed, separate into chunks, strata, systems and clusters
    # TODO for each strata get it's system
    pass

def find_clusters_affected_by_change():
    changed_systems = find_systems_affected_by_change()
    # TODO for each system get it's custers

def build(system):
    import platform
    log('building %s' % system)
    arch = platform.machine()
    if arch == 'aarch64':
        arch = 'armv8l64'
    exit_val =  subprocess.call(['sh','%s' % BUILD_SCRIPT, system, arch])
    log('build complete')
    return exit_val

def get_slave_name():
    ''' return the name the slave was created with by buildslave assuming the
    cwd looks something like
    /home/ciat/orchestration/orchenv-slave/slave/2__x86_64_Build/build/ybdenv
    '''
    dir = os.getcwd()
    return dir.split('/',)[-4]

def trigger_deploy(system):
    import platform
    import requests
    global url
    global buildslave_scripts_sha
    global definitions_sha
    global testing_sha
    global pipeline

    # Hacky skipping of attempting to deploy if this was an aarch64 build.
    # We don't yet have deployment set up,
    # and we don't want to cause an x86 deployment to happen.
    if platform.machine() == 'aarch64': return 0

    url = '%sbuild_complete' % ORCHE_URL
    if not system: return 0
    payload = {
        'system':system,
        'slave':get_slave_name(),
        'buildslave_scripts_sha':buildslave_scripts_sha,
        'definitions_sha':definitions_sha,
        'testing_sha':testing_sha,
        'pipeline':pipeline,
    }
    log('triggering deploy')
    r = requests.post(url,data=payload)
    return not r.ok

def do_build_deploy(system):
    build_exit_val = build(system)
    if build_exit_val: _exit(build_exit_val)
    return trigger_deploy(system)

def get_buildslave_scripts_sha():
    _cmd = ['git','ls-remote',BUILDSLAVE_SCRIPTS_REPO]
    _proc = subprocess.Popen(_cmd, stdout=subprocess.PIPE)
    _out, _err = _proc.communicate()
    return _out.split()[0]

def get_definitions_sha(ref):
    import os
    _cmd = ['git', 'log', ref, '--format=format:%H', '-1']
    _proc = subprocess.Popen(_cmd, stdout=subprocess.PIPE,cwd=DEFINITIONS_DIR)
    _out, _err = _proc.communicate()
    return _out.split()[0]

def get_testing_sha():
    _cmd = ['git','ls-remote',TESTING_REPO]
    _proc = subprocess.Popen(_cmd, stdout=subprocess.PIPE)
    _out, _err = _proc.communicate()
    return _out.split()[0]

if __name__ == '__main__':
    import sys
    global buildslave_scripts_sha
    global definitions_sha
    global testing_sha
    try:
        ref = sys.argv[1]
    except:
        ref = DEFAULT_REF
    try:
        definitions_sha = sys.argv[2]
    except:
        definitons_sha = None
    try:
        system = sys.argv[3]
    except:
        system = DEFAULT_SYSTEM
    global pipeline
    pipeline = sys.argv[4]
    if not definitions_sha or definitions_sha == 'HEAD':
        definitions_sha = get_definitions_sha('ref')
    buildslave_scripts_sha = get_buildslave_scripts_sha()
    testing_sha = get_testing_sha()
    if ref=="force":
        _exit(do_build_deploy(system))
    #_files_changed = files_changed(ref)
    systems_list = []
    #for f in _files_changed:
    #    if f in whitelist:
    #        _exit(do_build_deploy(system))
    _exit(do_build_deploy(system))
    log('nothing whitelisted changed. No build started.')
