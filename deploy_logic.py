
# Copyright (C) 2015  Codethink Limited
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 2 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program.  If not, see <http://www.gnu.org/licenses/>.

import subprocess, os

ORCHE_URL = 'http://127.0.0.1:8080/'
BUILD_SCRIPT = 'build_a_system.sh'
DEPLOY_SCRIPT = 'deploy_a_system.sh'
DEFINITIONS_DIR='definitions'
REF = "cu010-trove/br6/firehose-test-1"

whitelist = [
        'clusters/ciat-test-cluster.morph',
        'systems/base-system-x86_64-generic.morph',
        'strata/build-essential.morph',
        'strata/core.morph',
        'strata/foundation.morph',
        'strata/bsp-x86_64-generic.morph',
    ]

LOGFILE = os.path.expanduser("~/orchestration/trigger_log")

log_file = open(LOGFILE,'a')

def log(msg):
    ''' write message to log file with timestamp and script name '''
    import datetime
    global log_file
    dt = str(datetime.datetime.now()).split('.')[0]
    log_file.write("[%s] Deploy Trigger: %s\n" % (dt, msg))

def _exit(exit_val):
    if exit_val: log('exiting unhappily')
    exit(exit_val)

def deploy(cluster):
    log('deploying %s' % cluster)
    exit_val = subprocess.call(['sh',DEPLOY_SCRIPT,cluster])
    log('deployment complete')
    return exit_val

def trigger_testing(artefact,pipeline):
    import requests
    global url
    global testing_sha
    url = '%sdeploy_complete' % ORCHE_URL
    log("triggering testing")
    payload = {
            'artefact':artefact,
            'testing_sha':testing_sha,
            'buildslave_scripts_sha':buildslave_scripts_sha,
            'definitions_sha':definitions_sha,
            'pipeline':pipeline}
    r = requests.post(url,data=payload)
    return not r.ok

if __name__ == '__main__':
    import sys
    system = sys.argv[1]
    artefact = sys.argv[2]
    global testing_sha
    global buildslave_scripts_sha
    global definitions_sha
    testing_sha = sys.argv[3]
    buildslave_scripts_sha = sys.argv[4]
    definitions_sha = sys.argv[5]
    pipeline = sys.argv[6]
    # TODO the cluster should not be hardcoded
    deploy_exit_val = deploy('clusters/ciat-test-cluster.morph')
    if deploy_exit_val: _exit(deploy_exit_val)
    _exit(trigger_testing(artefact,pipeline))
