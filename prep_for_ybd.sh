#!/bin/sh
# Copyright (C) 2015  Codethink Limited
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 2 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program.  If not, see <http://www.gnu.org/licenses/>.

# Script to set up yum-y or apt-y machine for ybd use

if [ -x /usr/bin/apt ]; then
    apt-get install -y make automake gcc g++ linux-headers-$(uname -r) git gawk python-virtualenv
elif [ -x /usr/bin/yum ]; then
    yum install -y make automake gcc gcc-c++ kernel-devel git gawk python-virtualenv
else
    echo "Unsuported package manager, try apt or yum."
    exit 1
fi
