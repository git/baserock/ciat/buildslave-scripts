
# Copyright (C) 2015  Codethink Limited
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 2 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program.  If not, see <http://www.gnu.org/licenses/>.
set -ex
ARTEFACT=$1
TESTING_SHA=$2
BUILDSLAVE_SCRIPTS_SHA=$3
DEFINITIONS_SHA=$4
PIPELINE="$5"

ORCHE_URL='http://127.0.0.1:8080'
echo "[$(date "+%F %R")] Testing triggered. Artefact: $1" >> ../../../../trigger_log


### Now you can run the tester:

cd /archive
sudo mkdir -p testing
sudo chown -R $USER /archive/testing
cd /archive/testing
if [ -d ciat-tester ]; then
    sudo rm -rf ciat-tester
fi
git clone git://git.baserock.org/baserock/ciat/ciat-tester.git
cd ciat-tester
if [ "$TESTING_SHA" != "no_testing_sha_given" ]; then
    git checkout $TESTING_SHA
fi
PATH=$PATH:/usr/local/bin

### Copy systems.setup with info of the systems to test,
### openstack.setup to get OpenStack host details
### and multiple.test with the tests to run
### (This shouldn't be hardcoded here)
cp /archive/pedroalvarez/system.setup system.setup
cp /archive/pedroalvarez/openstack.setup openstack.setup
cp /archive/pedroalvarez/multiple.test multiple.test


### Change the URL of system.setup to point to ARTEFACT
sed -i -e "s/IMAGE/$ARTEFACT.raw/g" system.setup

### Run the tester.
### (These files shouldn't be hardcoded)
python -u tester --setup system.setup multiple.test

curl -X POST -F artefact=$ARTEFACT -F testing_sha=$TESTING_SHA -F buildslave_scripts_sha=$BUILDSLAVE_SCRIPTS_SHA -F definitions_sha=$DEFINITIONS_SHA -F pipeline=$PIPELINE $ORCHE_URL/testing_complete
