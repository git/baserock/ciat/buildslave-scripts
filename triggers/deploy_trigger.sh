
# Copyright (C) 2015  Codethink Limited
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 2 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program.  If not, see <http://www.gnu.org/licenses/>.
set -ex
SYSTEM=$1
BUILDNUMBER=$2
BUILDSLAVE_SCRIPTS_SHA=$3
DEFINITIONS_SHA=$4
TESTING_SHA=$5
PIPELINE="$6"
ARTEFACT="ciat-test-x86_64_$BUILDNUMBER"
echo "[$(date "+%F %R")] Deploy triggered with system $SYSTEM." >> ../../../../trigger_log
sed "s/ARTEFACT/$ARTEFACT/" ciat-test-cluster.morph > definitions/clusters/ciat-test-cluster.morph
git checkout $BUILDSLAVE_SCRIPTS_SHA
if [ -d ybd-env ]; then
    rm -rf ybd-env
fi
virtualenv --no-site-packages ybd-env
cd ybd-env
./bin/pip install pyyaml sandboxlib jsonschema requests
git clone -n https://github.com/devcurmudgeon/ybd.git
git --git-dir=./ybd/.git --work-tree=./ybd checkout 15.39
cp ../deploy_logic.py .
cp ../deploy_a_system.sh .
cp ../ybd.conf ybd
cp ../ybd.conf ybd/config
mv ../definitions .
cd definitions
git checkout $DEFINITIONS_SHA
cd ..
./bin/python deploy_logic.py $SYSTEM $ARTEFACT $TESTING_SHA "$DEFINITIONS_SHA" "$TESTING_SHA" "$PIPELINE"
