
# Copyright (C) 2015  Codethink Limited
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 2 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program.  If not, see <http://www.gnu.org/licenses/>.
set -ex
REF="$1"
SHA="$2"
SYSTEM="$3"
PIPELINE="$4"
echo "[$(date "+%F %R")] Builder triggered. Ref=\"$1\"" >> ../../../../trigger_log

if [ -d ybd-env ]; then
    rm -rf ybd-env
fi
virtualenv --no-site-packages ybd-env
cd ybd-env
./bin/pip install pyyaml sandboxlib jsonschema requests
git clone -n https://github.com/devcurmudgeon/ybd.git
git --git-dir=./ybd/.git --work-tree=./ybd checkout 15.39
cp ../builder_logic.py .
cp ../build_a_system.sh .
cp ../ybd.conf ybd
cp ../ybd.conf ybd/config
mv ../definitions .
./bin/python builder_logic.py "$REF" "$SHA" "$SYSTEM" "$PIPELINE"
