
# Copyright (C) 2015  Codethink Limited
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 2 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program.  If not, see <http://www.gnu.org/licenses/>.
set -ex
REPO="$1"
REF="$2"
PIPELINE="$3"
echo "[$(date "+%F %R")] Firehose triggered with repo: $1" >> ../../../../trigger_log
if [ -d src/firehose ]; then
    rm -rf src/firehose
fi
if [ -d src/cliapp ]; then
    rm -rf src/cliapp
fi
if [ -d src/morph ]; then
    rm -rf src/morph
fi
# Install firehose
git clone -b cu010-trove/br6/williamholland ssh://git@git.baserock.org/baserock/baserock/firehose.git ./src/firehose
git clone -b baserock/morph git://git.baserock.org/delta/cliapp.git ./src/cliapp
git clone -n git://git.baserock.org/baserock/baserock/morph.git ./src/morph
git --git-dir=./src/morph/.git --work-tree=./src/morph checkout 519a6460930c5ae08a9591035f72e88fd4db9b71
install -D -m755 /dev/stdin ./bin/morph <<EOF
#!/bin/sh
BASE="$(pwd)/src/morph"
PYTHONPATH="\${BASE}\${PYTHONPATH:+:\${PYTHONPATH}}"
export PYTHONPATH
exec "\$BASE/morph" "\$@"
EOF
SITE="$(python -m site --user-site)"
mkdir -p "$SITE"
if [ ! -L "$SITE"/cliapp ]; then
    ln -s ./src/cliapp/cliapp "$SITE"
fi
if [ ! -L "$SITE"/morphlib ]; then
    ln -s ./src/morph/morphlib "$SITE"
fi
../../../bin/pip install fs pylru
install -D -m644 /dev/stdin ~/.morph.conf <<'EOF'
[config]
trove-host = git.baserock.org
EOF
SLAVEBIN="$(pwd)/bin"
PATH="$PATH:$SLAVEBIN"
python firehose_call.py "$1"
